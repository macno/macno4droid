package org.macno.android.utils.display;

import android.content.Context;

public class ScreenUtil {

	public static int pixelsToSp(Context context, int px) {
	    float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
	    return (int)(px/scaledDensity);
	}
	
	public static int pixelsToDp(Context context, int px) {
		float logicalDensity = context.getResources().getDisplayMetrics().density;
		return (int) (px/logicalDensity + 0.5);
	}
	
	public static int dpToPixel(Context context, int dp) {
		float logicalDensity = context.getResources().getDisplayMetrics().density;
		return (int) (dp * logicalDensity + 0.5);
	}

}
