package org.macno.android.utils.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class FileUtil {

	public static boolean unpackZip(File destPath, File zip, boolean check) throws IOException {
		if(check && !FileUtil.isValidZip(zip)) {
			throw new IOException("File is corrupted!");
		}
		InputStream is = new FileInputStream(zip);
		return unpackZip(destPath, is);
	}
	
	public static boolean unpackZip(File destPath, File zip) throws IOException {
		return unpackZip(destPath,zip,true);
	}
	
	public static boolean unpackZip(File destPath, InputStream is) throws IOException {
		
		ZipInputStream zis;

		String filename;
		
		
		zis = new ZipInputStream(new BufferedInputStream(is));          
		ZipEntry ze;
		byte[] buffer = new byte[1024];
		int count;

		while ((ze = zis.getNextEntry()) != null) {
			// zapis do souboru
			filename = ze.getName();

			if(ze.isDirectory()) {
				File newFile = new File(destPath, filename);
				File parentNewFile = newFile.getParentFile();
				if(!parentNewFile.exists()) {
					parentNewFile.mkdirs();
				}
			} else {
				File newFile = new File(destPath, filename);
				File parentNewFile = newFile.getParentFile();
				if(!parentNewFile.exists()) {
					parentNewFile.mkdirs();
				}
				FileOutputStream fout = new FileOutputStream(newFile);

				// cteni zipu a zapis
				while ((count = zis.read(buffer)) != -1) {
					fout.write(buffer, 0, count);             
				}
				fout.close();               
				zis.closeEntry();
			}
		}

		zis.close();


		return true;
	}
	
	static boolean isValidZip(final File file) {
	    ZipFile zipfile = null;
	    try {
	        zipfile = new ZipFile(file);
	        return true;
	    } catch (ZipException e) {
	        return false;
	    } catch (IOException e) {
	        return false;
	    } finally {
	        try {
	            if (zipfile != null) {
	                zipfile.close();
	                zipfile = null;
	            }
	        } catch (IOException e) {
	        }
	    }
	}
	
	public static void deleteDirectory(File file) {

		final File to = new File(file.getAbsolutePath() + System.currentTimeMillis());
		file.renameTo(to);
		_deleteDirectory(to);
	}
	
	private static void _deleteDirectory(File file) {
		if(file.exists()){
			if(file.isDirectory()){
				File childrenFiles[] = file.listFiles();
				if(childrenFiles.length != 0){
					for(File childFile : childrenFiles){

						deleteDirectory(childFile);
					}
				}
			}

			try {
				boolean bool = file.delete();
				if(!bool) {
					// Dovrei fare qualcosa... cosa?
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
