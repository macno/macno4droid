package org.macno.android.utils.string;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

public class StringUtil {

	public static final String COMMA_SEPARATOR = ",";
	public static final String PIPE_SEPARATOR = "|";
	
	public static boolean isURL(String s) {
		ArrayList<String> schemas = new ArrayList<String>();
		schemas.add("http");
		schemas.add("https");
		return isURL(s,schemas);
	}
	
	public static boolean isURL(String s,ArrayList<String> schemas) {
//		StringBuilder schemasReg = new StringBuilder();
//		boolean first=true;
		for (String schema : schemas) {
//			if(!first) {
//				schemasReg.append("|");
//			} else {
//				first = false;
//			}
//			schemasReg.append(schema);
			if(s.startsWith(schema+"://")) {
				return true;
			}
		}
		return false;
//		String regex = "<\\b("+schemasReg.toString()+")://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>";
//		
//		try {
//            Pattern patt = Pattern.compile(regex);
//            Matcher matcher = patt.matcher(s);
//            return matcher.matches();
//        } catch (RuntimeException e) {
//        	return false;
//        }
	}

	public static String parseColor(String color) {
		String ret = "";
		if(color.startsWith("0x")) {
			ret = "#" + color.substring(2).toUpperCase(Locale.US);
		} else if(!color.startsWith("#")) {
			ret = "#" + color;
		} else {
			ret = color;
		}
		return ret;
	}
	
	public static ArrayList<String> arrayToList(String[] strings) {
		return new ArrayList<String>(Arrays.asList(strings));
	}
	
	public static ArrayList<String> stringToList(String string,String separator) {
		return StringUtil.arrayToList(string.split(separator));
	}
	
	public static String listToString(ArrayList<String> list) {
		StringBuilder ret = new StringBuilder();
		boolean first=true;
		for (String string : list) {
			if(first) {
				first=false;
			} else {
				ret.append(",");
			}
			ret.append(string);
		}
		return ret.toString();
	}
	
	public static String getHashString(MessageDigest digest) {
		StringBuilder builder = new StringBuilder();

		for (byte b : digest.digest()) {
			builder.append(Integer.toHexString((b >> 4) & 0xf));
			builder.append(Integer.toHexString(b & 0xf));
		}

		return builder.toString();
	}

	public static String getMd5(String str) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(str.getBytes());
			return getHashString(digest);
	    } catch (NoSuchAlgorithmException e) {
	      // This shouldn't happen.
	      throw new RuntimeException("No MD5 algorithm.");
	    }
	}
	
	public static String random(String[] strings) {
		ArrayList<String> astrings = new ArrayList<String>(strings.length);
		for (String string : strings) {
			astrings.add(string);
		}
		return random(astrings);
	}
	
	public static String random(ArrayList<String> strings) {
		if(strings == null || strings.size()< 1) {
			return null;
		}
		Collections.shuffle(strings);
		return strings.get(0);
	}
}
