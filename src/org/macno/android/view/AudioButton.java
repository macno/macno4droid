package org.macno.android.view;

import org.macno.android.utils.R;
import org.macno.android.utils.image.ImageUtil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class AudioButton extends ImageButton implements OnCompletionListener, View.OnClickListener, View.OnLongClickListener {

	private MediaPlayer mMediaPlayer;
	
	private Uri audioUri;
	
	private Bitmap mPlayBitmap,mStopBitmap;
	
	public AudioButton(Context context) {
		this(context,null);
	}
	
	public AudioButton(Context context,AttributeSet attr) {
		super(context,attr);
		init();
		setOnClickListener(this);
	}
	
	private void init() {
		setBackgroundColor(Color.TRANSPARENT);
		if(mPlayBitmap != null)
			setImageBitmap(mPlayBitmap);
		
	}
	
	public void setCustomIcons(int play, int stop) {
		if(play > 0) {
			mPlayBitmap = BitmapFactory.decodeResource(getResources(), play);
		}
		if(stop > 0) {
			mStopBitmap = BitmapFactory.decodeResource(getResources(), stop);
		}
		init();
	}
	
	public void setCustomIcons(String play, String stop) {
		if(play != null) {
			mPlayBitmap = ImageUtil.getBitmapFromFile(play);
		}
		if(stop != null) {
			mStopBitmap = ImageUtil.getBitmapFromFile(stop);
		}
		init();
	}
	
	public boolean isPlaying() {
		return (mMediaPlayer != null) ? mMediaPlayer.isPlaying() : false;
	}

	public void stop() {
		if(mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
			}
			mMediaPlayer.release();
			mMediaPlayer = null;
			setImageBitmap(mStopBitmap);
		}
		
	}
	
	public void changeState() {
		if(audioUri == null) {
			Toast.makeText(getContext(), R.string.error_no_audiofile, Toast.LENGTH_SHORT).show();
			return;
		}
		if(mMediaPlayer == null) {
			mMediaPlayer = MediaPlayer.create(getContext(), audioUri);
		}
		if(mMediaPlayer.isPlaying()) {
			if(mStopBitmap != null)
				setImageBitmap(mStopBitmap);
			mMediaPlayer.pause();			
		} else {
			if(mPlayBitmap != null)
				setImageBitmap(mPlayBitmap);
			mMediaPlayer.start();
		}
	}
	
	public void setAudioFile(Uri uri) {
		audioUri=uri;
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		setImageBitmap(mStopBitmap);
	}

	@Override
	public boolean onLongClick(View v) {
		if(mMediaPlayer != null) {
			mMediaPlayer.pause();
			mMediaPlayer.stop();
			mMediaPlayer.seekTo(0);
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		changeState();
	}

}
